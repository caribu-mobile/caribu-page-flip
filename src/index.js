import React from 'react';
import {
  PanResponder,
  View,
  StyleSheet,
  Platform,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import transformUtil from './transform-utils';
import { rotateX, rotateY, transformOrigin } from './old-transform-utils';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';
//import { rotateX, rotateY, transformOrigin, applyPerspective, multiplyInto, createIdentityMatrix } from './transform-utils';
import renderVerticalPage from './vertical-page';
import renderHorizontalPage from './horizontal-page';
const FRAME_SKIP = Platform.OS == 'ios' ? IS_IPHONE_X ? 2 : 2 : 2;
const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');
const IS_IPHONE_X = (() => {
  return (
    (Platform.OS === 'ios' &&
      ((SCREEN_HEIGHT === X_HEIGHT && SCREEN_WIDTH === X_WIDTH) ||
        (SCREEN_HEIGHT === X_WIDTH && SCREEN_WIDTH === X_HEIGHT))) ||
    ((SCREEN_HEIGHT === XSMAX_HEIGHT && SCREEN_WIDTH === XSMAX_WIDTH) ||
      (SCREEN_HEIGHT === XSMAX_WIDTH && SCREEN_WIDTH === XSMAX_HEIGHT))
  );
})();

const X_WIDTH = 375;
const X_HEIGHT = 812;
const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

class FlipPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      angle: 0,
      page: 0,
      halfHeight: 0,
      shouldGoNext: false,
      shouldGoPrevious: false,
      direction: '',
      flipState: true,
      pressing: false,
      ignoreMovement: false,
      forceSetPage: false,
      networkFlip: false
    };

    this.firstHalves = [];
    this.secondHalves = [];

    this.onLayout = this.onLayout.bind(this);
    this.renderPage = this.renderPage.bind(this);
  }

  //Network we receive a prop update from navigation.
  //This is different then a prop update from self

  //Some things are different.
  //Angle is changing when flipping yourself, while angle is 0 when someone else flips you
  //We can set a variable when we flip ourselfs.
  //this should work beause if page is the same the other client will ignore

  componentDidMount(){
    this.props.onRef(this);
  }



  componentDidUpdate(prevProps){
    if (prevProps.currentPage){

      if (this.props.currentPage != this.state.page){
        //console.log('component did update has currentpage, state doesnt equal prop');
        //If a page is let go halfway, this is a hacky way to fix. and still bind he page number to the codebase Something to do with when setState udpates...

        //If own client triggered page flip
        if (this.state.flipState || this.props.currentPage === 1){
          if ( this.props.currentPage > this.state.page){
            this.setState({page: this.props.currentPage, flipState: false});
            //console.log('set state next');
          }

          if ( this.props.currentPage < this.state.page){
            this.setState({page: this.props.currentPage, flipState: false});
            //console.log('set state previous');
          }
        }
        //if network triggered page flip
        else{
          // If forceSetPage caused by device rotation
          if (this.state.forceSetPage){
            this.setState({forceSetPage: false})
            return;
          }

          if (this.state.angle !== 0){
            return;
          }
          if ( this.props.currentPage > this.state.page){
            //don't trigger self in recursion.
            if (!(this.state.direction === 'left' && this.state.shouldGoNext && !this.state.shouldGoPrevious)){
              this.setState({
                angle:-20,
                direction: 'left',
                shouldGoNext: true,
                shouldGoPrevious: false,
                networkFlip: true
              }, this.resetHalves);
            }


          }
          if (this.props.currentPage < this.state.page){
            if (!(this.state.direction === 'right' && !this.state.shouldGoNext && this.state.shouldGoPrevious)){
              this.setState({
                angle:20,
                direction: 'right',
                shouldGoNext: false,
                shouldGoPrevious: true,
                networkFlip: true
              }, this.resetHalves);
            }
          }


        }
      }
    }

  }

  //this entire thing needs to be rewritten it makes little sense.
  shouldComponentUpdate(nextProps, nextState){

    // if (Math.abs(this.state.angle - nextState.angle) <= 3){
   //    return false;
   //  }

    if (nextState.angle === 0 ){
      return true;
    }
   if (this.state.page !== nextState.page){
     return true;
   }

  //if (nextState.page !== this.state.page){
//    return false;
 // }

   if (nextState.angle % FRAME_SKIP === 0){
     return false;
   }
     return true;
   }


  componentWillMount() {
   // this.resetTimer = 0;
    this.panResponder = PanResponder.create({
      onMoveShouldSetPanResponder: (evt, gestureState) => {
        if (gestureState.numberActiveTouches > 1 ){
          return;
        }
        /*
        untrap a zoomed in person.
        remove this code in BookViewerFlip
         <View
          pointerEvents={currentZoom !== 1 ? 'none' : 'auto'}
          // eslint-disable-next-line react-native/no-inline-styles
          style={{ flex: 1 }}
        >
        //console.log(this.props.currentZoom);
        //console.log(Math.abs(gestureState.vx));
        console.log(gestureState);
        if (this.props.currentZoom > 1.0 && Math.abs(gestureState.vx) <= 1.4){
          console.log('small pan  while zoomed in');
          return;
        }


        if (this.props.currentZoom > 1.0 && Math.abs(gestureState.vx) > 1.4){
          console.log('fast pan while zoomed in');
          this.props.onResetZoom();
          return;
        }
        */

        const { dx, dy } = gestureState;
        return Math.abs(dx) >= 1 && Math.abs(dy) >= 1;
      },
      onPanResponderMove: this.handlePanResponderMove.bind(this),
      onPanResponderRelease: this.handlePanResponderStop.bind(this),
    });
  }

  lastPage() {
    return this.props.children.length - 1;
  }

  isOnFirstPage() {
    return this.state.page === 0;
  }

  isOnLastPage() {
    return this.state.page === this.lastPage();
  }

  rotateFirstHalf(angle) {
    if (Platform.OS !== 'ios'){
      this.rotateFirstHalf2D(angle);
      return;
    }
    const {
      halfHeight,
      halfWidth,
      page,
    } = this.state;
    const { orientation } = this.props;
    const firstHalf = this.firstHalves[page];

    const matrix = transformUtil.createIdentityMatrix();
    const rotate = transformUtil.rotateY(-angle);
    transformUtil.origin(matrix, { x: halfWidth / 2, y: 0, z: 0 });
    transformUtil.applyPerspective(
      matrix,
      2400,
    );
    transformUtil.multiplyInto(matrix, matrix, rotate);
    transformUtil.origin(matrix, { x: halfWidth / 2, y: 0, z: 0 });
    if (firstHalf !== null){
    firstHalf.setNativeProps({
      transform: [
        { matrix },
      ],
    });
    }
  }

  rotateSecondHalf(angle) {
    if (Platform.OS !== 'ios'){
      this.rotateSecondHalf2D(angle);
      return;
    }
    const {
      halfHeight,
      halfWidth,
      page,
    } = this.state;
    const { orientation } = this.props;
    const secondHalf = this.secondHalves[page];

    const matrix = transformUtil.createIdentityMatrix();
    const rotate = transformUtil.rotateY(-angle);
    transformUtil.origin(matrix, { x: halfWidth / 2, y: 0, z: 0 });
    transformUtil.applyPerspective(
      matrix,
      2400,
    );
    transformUtil.multiplyInto(matrix, matrix, rotate);
    transformUtil.origin(matrix, { x: -halfWidth / 2, y: 0, z: 0 });

    if (secondHalf !== null){
    secondHalf.setNativeProps({
      transform: [
        { matrix },
      ],
    });
  }
  }


  rotateFirstHalf2D(angle) {
    const {
      halfHeight,
      halfWidth,
      page,
    } = this.state;
    const { orientation } = this.props;
    const firstHalf = this.firstHalves[page];

    let matrix = orientation === 'vertical' ? rotateX(angle) : rotateY(angle);
    const origin = orientation === 'vertical' ?
      { x: 0, y: halfHeight / 2, z: 0 } :
      { x: halfWidth / 2, y: 0, z: 0 };
    transformOrigin(matrix, origin);
    if (firstHalf !== null){
    firstHalf.setNativeProps({
      transform: [
        { matrix },
        { perspective: 100000 },
      ],
    });
  }
  }

  //
  //{scaleY: 1.00 + angle/1500},
  rotateSecondHalf2D(angle) {
    const {
      halfHeight,
      halfWidth,
      page,
    } = this.state;
    const { orientation } = this.props;
    const secondHalf = this.secondHalves[page];

    let matrix = orientation === 'vertical' ? rotateX(angle) : rotateY(angle);
    const origin = orientation === 'vertical' ?
      { x: 0, y: -halfHeight / 2, z: 0 } :
      { x: -halfWidth / 2, y: 0, z: 0 };
    transformOrigin(matrix, origin);
    if (secondHalf !== null){
    secondHalf.setNativeProps({
      transform: [
        { matrix },
        { perspective: 100000 },
      ],
    });
  }
  }

  relativeVelocity(vx){
    return(vx * 667/SCREEN_WIDTH)
  }

  handlePanResponderMove(e, gestureState) {
    const { dx, dy } = gestureState;
    const { direction, angle,page } = this.state;
    const { orientation, loopForever } = this.props;
    const dn =  dx;
    if (!this.state.pressing){
    this.setState({pressing: true});
    ReactNativeHapticFeedback.trigger('impactLight', options);
    }
    if (this.resetTimer){
      //console.log('reset timer ignore handlePanResponderMove');
      return;
    }
    if (this.state.ignoreMovement){
      //console.log('ignore movement handlePanResponderMove');
      return;
    }

    if (page === 1 && gestureState.vx >=0){
      return
    }
   // console.log(gestureState);
   //goes from two to one which is expected since swiping finger is let go.
    //console.log('number active touches');
    //console.log(e.nativeEvent.touches);
    //console.log(gestureState.numberActiveTouches);

    if (gestureState.numberActiveTouches > 1 || Math.abs(gestureState.vx) < 0.03) {
      return;
    }


    const relativevx = this.relativeVelocity(gestureState.vx);
   // console.log(gestureState.vx);


    //quick flick left
    //console.log(angle);
    //if (Math.abs(angle) !== 180){
      // && angle>=-90 && Math.abs(angle) !== 180 && angle!==0
    if ((this.lastPage() <= page+1)){
      if (angle <= -10){
        this.setState({
          shouldGoNext: true,
          shouldGoPrevious: false,
        }, this.resetHalves);
        return;
      }
    }

    //console.log(angle);
    if (relativevx <= -0.65){
      if (angle <= -10){
      //console.log('handlePanResponderMove flick');
      this.setState({
        shouldGoNext: true,
        shouldGoPrevious: false,
      }, this.resetHalves);
      return;
      }
    }

    //quick flick right
    //&& angle<=90 && Math.abs(angle) !== 180 && angle!==0
    if (relativevx >= 0.65){
      if (angle >= 10){
      //console.log('handlePanResponderMove flick');
      if (page !== 1){
      this.setState({
        shouldGoNext: false,
        shouldGoPrevious: true,
      }, this.resetHalves);
      return;
    }
    }
    }


  //}
    let longSide = SCREEN_WIDTH;
    if (SCREEN_HEIGHT > SCREEN_WIDTH){
      longSide = SCREEN_HEIGHT;
    }
    let langle = longSide > 1000 ? (dn / (longSide/1.33) * 180) : longSide > 750  ? (dn / (longSide/1.6) * 180) : longSide > 640 ? (dn / (longSide/1.5) * 180) : (dn / (longSide/2.3) * 180) ;


    if (langle < 0) {
      langle = Math.max(-180, langle);
    } else {
      langle = Math.min(180, langle);
    }

    let nextDirection = direction;

    if (dn < 0 && direction === '') {
      nextDirection = orientation === 'vertical' ? 'top' : 'left';
    } else if (dn > 0 && direction === '') {
      nextDirection = orientation === 'vertical' ? 'bottom' : 'right';
    }

    this.setState({ direction: nextDirection });

    if (dn < 0 && (nextDirection === 'top' || nextDirection === 'left')) {
      if (this.isOnLastPage() && !loopForever) {
        langle = Math.max(langle, -30);
      }

      this.rotateSecondHalf(langle);

      this.setState({
        angle: langle,
      });
    } else if (dn > 0 && (nextDirection === 'bottom' || nextDirection === 'right')) {
      if (this.isOnFirstPage() && !loopForever) {
        langle = Math.min(langle, 30);
      }

      this.rotateFirstHalf(langle);

      this.setState({
        angle: langle,
      });
    }
  }

  resetHalves() {
    const { loopForever, children, onFinishBook, onCurrentPageChange } = this.props;
    const pages = children.length;
    const {
      angle,
      direction,
      shouldGoNext,
      shouldGoPrevious,
      page,
    } = this.state;

    const firstHalf = this.firstHalves[page];
    const secondHalf = this.secondHalves[page];
    const finish = () => {
      this.setState({ direction: '', flipState: true });
      if (this.state.pressing){
        this.setState({ignoreMovement: true})
      }
      if (shouldGoNext) {

        //lag logic for some reason.
        // page: loopForever && this.isOnLastPage() ? 0 : page + 1,
        this.setState({
          angle: 0,

        }, () => {
          //firstHalf.setNativeProps({ transform: [] });
          //secondHalf.setNativeProps({ transform: [] });
        });

        onCurrentPageChange(page+1, this.state.networkFlip);
        this.setState({networkFlip: false})
        if (this.lastPage() <= page+1){
        setTimeout(() => {
          onFinishBook();
        }, 10);
        }
      } else if (shouldGoPrevious) {
        this.setState({
          angle: 0,
        }, () => {
          //firstHalf.setNativeProps({ transform: [] });
          //secondHalf.setNativeProps({ transform: [] });
        });
        onCurrentPageChange(page-1, this.state.networkFlip);
        this.setState({networkFlip: false})
      }
    };

    // Already swiped all the way
    if (Math.abs(angle) === 180) {
      finish();
      return;
    }

    let targetAngle;
    if (angle < -90) {
      targetAngle = -180;
    } else if (angle > 90) {
      targetAngle = 180;
    } else {
      if (shouldGoNext) {
        targetAngle = -180;
      }else if (shouldGoPrevious){
        targetAngle = 180;
      }else{
      targetAngle = 0;
      }
    }

    this.resetTimer = setInterval(() => {
      let { angle } = this.state;
      angle += angle < targetAngle ? 7 : -7;

      if (angle < 0) {
        angle = Math.max(angle, -180);
      } else {
        angle = Math.min(angle, 180);
      }

      let matrix = transformUtil.rotateX(angle);

      if (angle < 0) {
        this.rotateSecondHalf(angle);
      } else {
        this.rotateFirstHalf(angle);
      }

      this.setState({ angle });

      if (
        (targetAngle < 0 && angle <= targetAngle) || // Flip second half to top
        (targetAngle === 0 && Math.abs(angle) <= 5) ||
        (this.lastPage() <= page+1) ||
        (targetAngle > 0 && angle >= targetAngle) // Flip first half to bottom
      ) {
        clearInterval(this.resetTimer);
        this.resetTimer = false;
        //this.resetTimer = 0;

        if (direction === 'top' || direction === 'left' || direction === '') {
          this.rotateSecondHalf(targetAngle);
        } else if (direction === 'bottom' || direction === 'right' || direction === '') {
          this.rotateFirstHalf(targetAngle);
        }
        //this.setState({page: this.props.currentPage+ 1})
       finish();
      }
    }, 15);
  }

  handlePanResponderStop(e, gestureState) {
    const { dx, dy } = gestureState;
    const { angle, page, direction } = this.state;
    const { orientation } = this.props;
    const dn = orientation === 'vertical' ? dy : dx;
    const absAngle = Math.abs(angle);
    this.setState({pressing: false, ignoreMovement: false});
    if (this.resetTimer){
      return;
    }

    if (angle === 0) {
      return;
    }


    const relativevx = this.relativeVelocity(gestureState.vx);

    if (dn === 0) {
      const { onPress } = this.props.children[page].props;
      if (typeof onPress === 'function') {
        onPress();
      }
    }

    //quick flick left


    if (direction === 'left'){
    if (relativevx <= -0.3){
      if (angle < 0){
      //console.log('handlePanResponderStop flick');
      this.setState({
        shouldGoNext: true,
        shouldGoPrevious: false,
      }, this.resetHalves);
      return;
      }
    }
  }


    //quick flick right
    if (direction === 'right'){
    if (relativevx >= 0.3){
      if (angle > 0){
      //console.log('handlePanResponderStop flick');
      if (page !== 1){
      this.setState({
        shouldGoNext: false,
        shouldGoPrevious: true,
      }, this.resetHalves);
      return;
    }
    }
    }
  }


    //resetHalves called after setstate resolved
    this.setState({
      shouldGoNext: absAngle > 90 && (direction === 'top' || direction === 'left'),
      shouldGoPrevious: absAngle > 90 && (direction === 'bottom' || direction === 'right'),
    }, this.resetHalves);

  }

  onLayout(e) {
    const { layout } = e.nativeEvent;
    const { width, height } = layout;
    const halfHeight = height / 2;
    const halfWidth = width / 2;

    this.setState({
      halfHeight,
      halfWidth,
    });
  }

  renderVerticalPage(previousPage, thisPage, nextPage, index) {
    const {
      angle,
      page,
      halfHeight,
      direction,
    } = this.state;

    const height = { height: halfHeight * 2 };

    const absAngle = Math.abs(angle);

    const secondHalfPull = {
      marginTop: -halfHeight,
    };

    const setViewCallback = (view) => this.firstHalves[index] = view;

    return renderVerticalPage(
      absAngle,
      page,
      halfHeight,
      direction,
      height,
      secondHalfPull,
      styles,
      index,
      this,
      previousPage,
      thisPage,
      nextPage,
    );
  }

  renderHorizontalPage(previousPage, thisPage, nextPage, index) {
    const {
      angle,
      page,
      halfHeight,
      halfWidth,
      direction,
    } = this.state;

    const width = { width: halfWidth * 2 };

    const absAngle = Math.abs(angle);

    const secondHalfPull = {
      marginLeft: -halfWidth,
    };

    return renderHorizontalPage(
      absAngle,
      page,
      halfWidth,
      direction,
      width,
      secondHalfPull,
      styles,
      index,
      this,
      previousPage,
      thisPage,
      nextPage,
    );
  }

  renderPage(component, index) {
   // console.log(`render page ${index}`);
    const { halfWidth, page, angle } = this.state;
    const { children, orientation, loopForever } = this.props;
    const pages = children.length;

    const thisPage = component;
    const nextPage = index + 1 < pages ? children[index + 1] : (loopForever ? children[0] : null);
    const previousPage = index > 0 ? children[index - 1] : (loopForever ? children[pages - 1] : null);

    //for each page we're rendering 3 pages.

    if (page !== index){
      return null;
    }
    if (page && Math.abs(page - index) > 0) {
      return null;
    }

    return this.renderHorizontalPage(previousPage, thisPage, nextPage, index);
    //return this.renderHorizontalPage(this.state.direction === 'right' ? previousPage : null, thisPage, this.state.direction === 'left' ? nextPage : null, index);

  }

  //setting a key on view here causes lag because it forces a re-render but Idk if this was require for network..?
  render() {
    const { children } = this.props;
    return (
      <View
        style={styles.container}
        {...this.panResponder.panHandlers}
        onLayout={this.onLayout}
      >
        {children.map(this.renderPage)}
      </View>
    );
  }

  forceSetPage(page) {
    if (this.lastPage() <= page+1){
      setTimeout(() => {
        this.props.onFinishBook();
      }, 10);
      return
    }
    this.setState({page: page, forceSetPage: true
    }, ()=>{
      this.setState({flipState: true})
      this.props.onCurrentPageChange(page, false);
    })
  }
};

FlipPage.propTypes = {
  orientation: PropTypes.oneOf(['horizontal', 'vertical']),
  loopForever: PropTypes.bool,
};

FlipPage.defaultProps = {
  orientation: 'vertical',
  loopForever: false,
};

class FlipPagePage extends React.PureComponent {
  render() {
    const { children, style, onPress } = this.props;
    const defaultStyle = {
      backgroundColor: 'white',
      height: '100%',
      width: '100%',
    };
    const finalStyle = Object.assign({}, defaultStyle, style);

    /*
     <ImageBackground source={require('./bookbg.png')} style={finalStyle}>
     */
    return (
      <View style={finalStyle}>
        {children}
      </View>
    )
  }
};

export {
  FlipPage as default,
  FlipPagePage
};
